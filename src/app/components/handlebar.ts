import { Component } from '@angular/core';

@Component({
	selector: 'handlebar-generator',
	template: 'dummy.html'
})


export class Handlebar {

	isEmpty(str) {
		return (!str || 0 === str.length || str === 'null');
	}
	isNotEmpty(str) {
		return !this.isEmpty(str);
	}
	checkImagePath(imagePath) {
		if (this.isEmpty(imagePath)) {
			return false;
		} else {
			if (imagePath.indexOf('https://www.gravatar.com/avatar/') != -1) {
				return false;
			}
			return true;
		}
	}
	jsonString(str) {
		try {
			return JSON.parse(str);
		} catch (e) {
			return str;
		}
	}
	amountValidator(amount){
		let result = false;
		let regexObj = /^\$?[0-9]?((\.[0-9]+)|([0-9]+(\.[0-9]+)?))$/;
		
		if (!regexObj.test(amount)) {
			result = true;
		} 
		return result;
	}
	mobileValidator(number) {
		let string = "valid";
		let regexObj = /^[0-9-()]*$/;

		if (!regexObj.test(number)) {
			string = " Mobile Number is not Valid.<br/>"
		} else if (number.length < 8 || number.length > 14) {
			string = " Mobile Number Length Between 8 to 14 in Digits.<br/>";
		}
		return string;
	}
	PincodeValidator(number) {
		let string = "valid";
		let regexObj = /^[0-9]*$/;
		if (!regexObj.test(number)) {
			string = " pincode is not Valid.<br/>"
		} else if (number.length > 4 && number.length < 11) {
			var validPin = number * 1;
			if (validPin === 0) {
				string = " pincode is not Valid.<br/>"
			}
		} else {
			string = " pincode is not Valid.<br/>"
		}
		return string;
	}
	emailValidator(email) {
		let string = "valid";
		let filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if (email !== "") {
			if (!filter.test(email)) {
				string = " Email is not Valid.<br/>"
			}
		}
		return string;
	}
	nameValidator(inputtxt, fieldName, fieldMinLength, fieldMaxLength) {
		let string = "valid";
		let letters = /^[a-zA-Z-,.]+(\s{0,1}[a-zA-Z-, ])*$/;
		if (!inputtxt.match(letters)) {
			string = fieldName + " must contain only letters, or dashes.<br>";
		}
		if (inputtxt.charAt(0) == " ") {
			string = fieldName + " first letter can't be space<br>";
		}
		if (inputtxt.length > fieldMaxLength || inputtxt.length < fieldMinLength) {
			string = fieldName + " should be within " + fieldMinLength + " to " + fieldMaxLength + " character<br>";
		}
		return string;
	}


	getGestationAge(dateString) {
		let date1 = new Date(dateString.substring(0, 4),
			dateString.substring(5, 7) - 1,
			dateString.substring(8, 10)
		);

		let returnTxt = "";
		let date2 = new Date();
		if (date1.getTime() >= date2.getTime()) {
			returnTxt = "0 Day(s) ";
		} else {
			const age = this.getAge(dateString);
			if (age) {
				if (age["years"] > 0) {
					returnTxt = age["years"] + " Year(s) ";
				}
				if (age["months"] > 0) {
					returnTxt += age["months"] + " Month(s) ";
				}
				if (age["days"] > 0) {
					let diffDays = age["days"];
					let weeks = Math.floor(diffDays / 7);
					let days = Math.floor(diffDays % 7);
					if (weeks > 0) {
						returnTxt += weeks + " Week(s) ";
					}
					if (days > 0) {
						if (weeks > 0) {
							returnTxt += "and ";
						}
						returnTxt += days + " Day(s) ";
					}
				}
				if (this.isEmpty(returnTxt)) {
					returnTxt = "0 Day(s) ";
				}

			}

		}
		return returnTxt;
	}
	getAge(dateString) {
		let now = new Date();
		let yearNow = now.getFullYear();
		let monthNow = now.getMonth();
		let dateNow = now.getDate();

		let dob = new Date(dateString.substring(0, 4),
			dateString.substring(5, 7) - 1,
			dateString.substring(8, 10)
		);
		let age = {};
		if (now.getTime() > dob.getTime()) {
			let yearDob = dob.getFullYear();
			let monthDob = dob.getMonth();
			let dateDob = dob.getDate();
			let yearAge = 0;
			let monthAge = 0;
			let dateAge = 0;

			yearAge = yearNow - yearDob;
			if (monthNow >= monthDob)
				monthAge = monthNow - monthDob;
			else {
				yearAge--;
				monthAge = 12 + monthNow - monthDob;
			}

			if (dateNow >= dateDob)
				dateAge = dateNow - dateDob;
			else {
				monthAge--;
				dateAge = (31 + dateNow) - dateDob;
				if (monthAge < 0) {
					monthAge = 11;
					yearAge--;
				}
			}

			age = {
				years: yearAge,
				months: monthAge,
				days: dateAge
			};
		} else {
			age = {
				years: 0,
				months: 0,
				days: 0
			};
		}

		return age;
	}

}