import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Handlebar } from '../handlebar';
import { ToastServiceData } from 'src/app/service/toast-service';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-receive-wallet-payment',
  templateUrl: './receive-wallet-payment.component.html',
  styleUrls: ['./receive-wallet-payment.component.scss'],
})
export class ReceiveWalletPaymentComponent implements OnInit {
  text: string;
  walletAmount: any;
  paymentModes: any;
  recievePayment: any = {
    invoiceRef: '',
    invoiceId: '',
    paymntAmount: '',
    notes: '',
    dueDate: '',
    careCenter: '0',
    paymentType: '',
    paymentRef: '',
    sharedWith: "Owner and Admin",
    sharedWithSource: '',
    externalSourceEmail: '',
    externalSourceMobile: '',
    paymntNotes: ''
  }
  careCenterId: any;
  patient: any;
  showSpinner: boolean = true;

  constructor(private navParams: NavParams,
    private modalCtrl: ModalController,
    private handlebar: Handlebar,
    private toast: ToastServiceData,
    private authService: AuthService) {
    console.log('Hello RecieveWalletPaymentComponent Component');
    this.text = 'Hello World';
    this.walletAmount = this.navParams.data.wallet;
    this.patient = this.navParams.data.patient;
    this.careCenterId = this.patient.registeredAt.id;
    this.getPaymentModes();
  }

  dismiss(isUpdated, data?: any) {
    this.modalCtrl.dismiss(data, isUpdated);
  }

  getPaymentModes() {
    this.authService.getPaymentModes().then((data) => {
      this.paymentModes = data;
    });
  }
  payWallet() {
    this.showSpinner = !this.showSpinner;
  }
  addWalletAmount() {
    this.showSpinner = !this.showSpinner;
    let validData: boolean = true;
    this.recievePayment.paymentType = 'advance';
    this.recievePayment.careCenter = this.careCenterId;
    validData = this.validateWalletData();
    if (validData) {
      var dataArr = [{
        "paymentAmount": this.recievePayment.paymntAmount,
        "paymentMode": this.recievePayment.paymentMode,
        "paymntNotes": this.recievePayment.paymntNotes
      }];
      this.recievePayment.paymentmodes = dataArr;
      this.recievePayment.uhid = this.patient.patientUhid;
      this.authService.addWalletAmount(this.recievePayment, false).then(returndata => {
        this.showSpinner = !this.showSpinner;
        if (returndata) {
          this.presentToast("Wallet Amount Added Successfully");
          this.dismiss(true);
        } else {
          this.presentToast("Something went wrong please try again");
        }
      });
    } else {
      this.showSpinner = !this.showSpinner;
    }
  }
  validateWalletData() {
    let errMsg = "";
    if (this.handlebar.isEmpty(this.recievePayment.paymntAmount) || this.recievePayment.paymntAmount === "0") {
      errMsg = "Payment Amount is not valid";
      return this.presentToast(errMsg);
    }
    if (this.handlebar.isEmpty(this.recievePayment.paymentMode)) {
      errMsg = "Payment Mode cannot be empty";
      return this.presentToast(errMsg);
    }
    if (this.handlebar.amountValidator(this.recievePayment.paymntAmount)) {
      errMsg = "Payment Amount is not valid";
      return this.presentToast(errMsg);
    }
    return true;
  }
  presentToast(msg) {
    if (this.handlebar.isEmpty(msg)) {
      return true;
    } else {
      this.toast.presentToast(msg);
      return false;
    }
  }
  ngOnInit() { }

}
