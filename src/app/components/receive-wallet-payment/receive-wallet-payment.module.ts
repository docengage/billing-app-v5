import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReceiveWalletPaymentComponent } from './receive-wallet-payment.component';

@Component({
  selector: 'app-receive-wallet-payment',
  templateUrl: './receive-wallet-payment.component.html',
  styleUrls: ['./receive-wallet-payment.component.scss'],
})
@NgModule({
  imports: [CommonModule, FormsModule, IonicModule,],
  declarations: [ReceiveWalletPaymentComponent],
  exports: [ReceiveWalletPaymentComponent]
})
export class ReceiveWalletPaymentComponentModule { }
