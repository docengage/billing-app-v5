import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { Handlebar } from '../handlebar';
import { AuthService } from 'src/app/service/auth.service';
import { ToastServiceData } from 'src/app/service/toast-service';
import * as moment from 'moment';
import { ReceivePaymentPage } from 'src/app/receive-payment/receive-payment.page';

@Component({
  selector: 'app-patient-invoice-list',
  templateUrl: './patient-invoice-list.component.html',
  styleUrls: ['./patient-invoice-list.component.scss'],
})
export class PatientInvoiceListComponent implements OnInit {
  text: string;
  patient: any;
  currentPage: any = 1;
  totalPages: any;
  invoices: any;
  noRecordFound: any = "";
  invOldIndex: any = -1;
  constructor(private navParams: NavParams,
    private handlebar: Handlebar,
    private authService: AuthService,
    private modalCtrl: ModalController,
    private toastServ: ToastServiceData) {
    console.log('Hello PatientInvoiceListComponent Component');
    this.text = 'Hello World';
    this.patient = this.navParams.data.patient;
    this.invoices = [];
    this.getAllinvoices();
  }
  getAllinvoices() {
    this.authService.findInvoice(0, this.currentPage, this.patient.patientUhid).then((data) => {
      if (data && data.invoice.length !== 0) {
        var invoices = data.invoice;
        for (let i = 0, I = invoices.length; i < I; i += 1) {
          invoices[i].showContent = false;
          invoices[i].invoiceItems = 0;
          if (this.handlebar.isNotEmpty(invoices[i].due_date1)) {
            try {
              invoices[i].formatedDate = moment(invoices[i].due_date1).format("DD-MMM-YYYY");
            } catch (error) {
              invoices[i].formatedDate = "";
            }
          }
          this.invoices.push(invoices[i]);
          this.totalPages = this.invoices[i].full_count / 15;
          this.noRecordFound = "";
        }
      } else {
        this.invoices = [];
        this.noRecordFound = "No Invoices available for Pay";
      }

    });
  }
  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }
  getInvoiceItems(invoiceId, index) {
    this.authService.getInvoiceItems(invoiceId).then((data) => {
      this.invoices[index].invoiceItems = data[0].length;
    });
  }
  showInvContentData(index) {
    this.invoices[index].showContent = !this.invoices[index].showContent
    let invoiceId = this.invoices[index].invoice_id;
    if (this.invoices[index].showContent) {
      this.getInvoiceItems(invoiceId, index);
    }
    if (this.invOldIndex !== -1 && this.invOldIndex !== index) {
      this.invoices[this.invOldIndex].showContent = false;
    }
    this.invOldIndex = index;
  }
  async recievePayment(item) {
    let data = {
      item: item
    }
    let recievePayment = await this.modalCtrl.create({ component: ReceivePaymentPage, componentProps: data });
    await recievePayment.present();
    recievePayment.onDidDismiss().then((isUpdate) => {
      if (isUpdate) {
        this.invoices = [];
        this.getAllinvoices();
      }
    });
  }
  dismiss(isUpdated, data?: any) {
    this.modalCtrl.dismiss(data, isUpdated);
  }

  doRefresh(event) {
    this.invoices = [];
    this.getAllinvoices();
    setTimeout(() => {
      event.target.complete();
      this.toastServ.presentToast("Invoice List Refreshed");
    }, 1000);
  }

  doInfinite(event) {
    this.currentPage = Number(this.currentPage) + 1;
    return setTimeout(() => {
      setTimeout(() => {
        if (this.currentPage <= this.totalPages) {
          this.getAllinvoices();
        }
        event.target.complete();
      }, 500);
    })
  }
  ngOnInit() { }

}
