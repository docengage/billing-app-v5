import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PatientInvoiceListComponent } from './patient-invoice-list.component';

@Component({
  selector: 'app-patient-invoice-list',
  templateUrl: './patient-invoice-list.component.html',
  styleUrls: ['./patient-invoice-list.component.scss'],
})
@NgModule({
  imports: [CommonModule, FormsModule, IonicModule,],
  declarations: [PatientInvoiceListComponent],
  exports: [PatientInvoiceListComponent]
})
export class PatientInvoiceListComponentModule {}
