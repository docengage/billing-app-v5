import { NgModule } from '@angular/core';
import { Handlebar } from './handlebar';
import { DynamicFormCommon } from './dynamicform-common';
import { ColorgeneratorComponent } from 'src/app/components/colorgenerator/colorgenerator.component';
import { NewTxtImgComponent } from './new-txt-img/new-txt-img.component';


@NgModule({
    declarations: [ColorgeneratorComponent, Handlebar, DynamicFormCommon, NewTxtImgComponent],
    imports: [],
    exports: [ColorgeneratorComponent, Handlebar, DynamicFormCommon, NewTxtImgComponent]
})
export class ComponentsModule { }
