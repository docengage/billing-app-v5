import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';
import { ToastServiceData } from '../service/toast-service';
import * as moment from 'moment';


@Component({
  selector: 'app-receive-payment',
  templateUrl: './receive-payment.page.html',
  styleUrls: ['./receive-payment.page.scss'],
})
export class ReceivePaymentPage implements OnInit {
  invoiceToggel: any;
  invoiceInfo: any;
  invoiceItems: any;
  paymentModes: any;
  targetDate: any;
  recievePayment: any = {
    invoiceRef: '',
    invoiceId: '',
    paymntAmount: '',
    notes: '',
    dueDate: '',
    careCenter: '0',
    paymentType: '',
    paymentRef: '',
    sharedWith: "Owner and Admin",
    sharedWithSource: '',
    externalSourceEmail: '',
    externalSourceMobile: '',
    paymntNotes: ''
  }
  walletAmount: any;
  addToWallet: boolean = false;
  minDate: any = moment().format("YYYY-MM-DD");
  constructor(public navCtrl: NavController,
    public modalController: ModalController,
    private authService: AuthService,
    private handlebar: Handlebar,
    private toast: ToastServiceData,
    public navParams: NavParams) {
    this.invoiceInfo = this.navParams.data.item;
    this.getInvoiceItems();
    this.getPaymentModes();
    this.getWalletAmount();
  }
  ngOnInit() {
    
  }
  onSearchInv() {
    alert(this.invoiceToggel);
  }
  getInvoiceItems() {
    this.authService.getInvoiceItems(this.invoiceInfo.invoice_id).then((data) => {
      this.invoiceItems = data;
    });
  }
  getPaymentModes() {
    this.authService.getPaymentModes().then((data) => {
      this.paymentModes = data;
    });
  }
  getWalletAmount() {
    this.authService.getWalletAmount(this.invoiceInfo.uhid).then((data) => {
      this.walletAmount = data;
      this.recievePayment.invoiceRef = this.invoiceInfo.invoice_ref;
      this.recievePayment.invoiceId = this.invoiceInfo.invoice_id;
      this.recievePayment.paymntAmount = this.invoiceInfo.balance_amount;
      if(this.handlebar.isEmpty(this.invoiceInfo.due_date1)){
        this.targetDate = moment().format('YYYY-MM-DD');
      } else {
      this.targetDate = moment(this.invoiceInfo.due_date1).format('YYYY-MM-DD');
      }
    });
  }
  settingDueDate() {
    if (this.handlebar.isNotEmpty(this.recievePayment.paymntAmount) && this.recievePayment.paymntAmount < this.invoiceInfo.balance_amount) {
      if (this.handlebar.isNotEmpty(this.recievePayment.dueDate)) {
        this.targetDate = moment().format("DD-MM-YYYY");
      }
    }
  }
  addWalletAmount() {
    let validData: boolean = true;
    this.recievePayment.paymentType = 'advance';
    this.recievePayment.careCenter = this.invoiceInfo.care_center_id;
    validData = this.validateWalletData();
    if (validData) {
      var dataArr = [{
        "paymentAmount": this.recievePayment.paymntAmount,
        "paymentMode": this.recievePayment.paymentMode,
        "paymntNotes": this.recievePayment.paymntNotes
      }];
      this.recievePayment.paymentmodes = dataArr;
      this.recievePayment.uhid = this.invoiceInfo.uhid;
      this.authService.addWalletAmount(this.recievePayment, false).then(returndata => {
        this.presentToast("Wallet Amount Added Successfully");
        this.dismiss(true);
      });
    }
  }
  validateWalletData() {
    let errMsg = "";
    if (this.handlebar.isEmpty(this.recievePayment.paymntAmount)) {
      errMsg = "Payment Amount is not valid";
      return this.presentToast(errMsg);
    }
    if (this.handlebar.isEmpty(this.recievePayment.paymentMode)) {
      errMsg = "Payment Mode cannot be empty";
      return this.presentToast(errMsg);
    }
    return true;
  }
  payBill() {
    let validData: boolean = true;
    if (moment(this.minDate).isAfter(this.targetDate)){
      this.recievePayment.dueDate = moment().format('YYYY-MM-DD');
    }else{
    this.recievePayment.dueDate = moment(this.targetDate).format('YYYY-MM-DD');
    }
    this.recievePayment.paymentType = 'invoice';
    validData = this.validate();
    if (validData) {
      var dataArr = [{
        "paymentAmount": this.recievePayment.paymntAmount,
        "paymentMode": this.recievePayment.paymentMode,
        "paymntNotes": this.recievePayment.paymntNotes
      }];
      this.recievePayment.paymentmodes = dataArr;
      this.recievePayment.uhid = this.invoiceInfo.uhid;
      this.authService.payInvoiceBill(this.recievePayment, false).then(returndata => {
        this.presentToast("Payment Completed Successfully");
        this.dismiss(true);
      });
    }
  }
  presentToast(msg) {
    if (this.handlebar.isEmpty(msg)) {
      return true;
    } else {
      this.toast.presentToast(msg);
      return false;
    }
  }
  validate() {
    let errMsg = "";
    if (this.handlebar.isEmpty(this.recievePayment.paymntAmount) || this.recievePayment.paymntAmount === "0" || this.recievePayment.paymntAmount > this.invoiceInfo.balance_amount) {
      errMsg = "Payment Amount is not valid";
      return this.presentToast(errMsg);
    }
    if (this.handlebar.isEmpty(this.recievePayment.paymentMode)) {
      errMsg = "Payment Mode cannot be empty";
      return this.presentToast(errMsg);
    }
    if (this.handlebar.isNotEmpty(this.recievePayment.paymentMode) && this.recievePayment.paymentMode === "Settlement against Advance") {
      if (this.recievePayment.paymntAmount > this.walletAmount) {
        errMsg = "Payment Amount cannot be greater than Wallet Amount";
        return this.presentToast(errMsg);
      }
    }
    if (this.recievePayment.paymentMode === "Coupon Code") {
      if (this.handlebar.isEmpty(this.recievePayment.paymntNotes)) {
        errMsg = "Enter payment details.";
        return this.presentToast(errMsg);
      }
    }
    if (this.handlebar.isNotEmpty(this.recievePayment.paymntAmount) && this.recievePayment.paymntAmount < this.invoiceInfo.balance_amount) {
      if (this.handlebar.isEmpty(this.recievePayment.dueDate) || this.recievePayment.dueDate === "Invalid date" || (!moment(this.recievePayment.dueDate).isAfter() && !moment(this.recievePayment.dueDate).isSame(this.minDate))) {
        errMsg = "Please Select valid Due Date";
        return this.presentToast(errMsg);
      }
    }
    if (this.handlebar.amountValidator(this.recievePayment.paymntAmount)) {
      errMsg = "Payment Amount is not valid";
      return this.presentToast(errMsg);
    }
    return this.presentToast(errMsg);
  }

  dismiss(isUpdated) {
    this.modalController.dismiss(isUpdated);
  }

  

}
