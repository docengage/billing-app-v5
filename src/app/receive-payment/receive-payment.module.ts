import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { Component, Input } from '@angular/core';

import { ReceivePaymentPageRoutingModule } from './receive-payment-routing.module';

import { ReceivePaymentPage } from './receive-payment.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReceivePaymentPageRoutingModule,
    Component
  ],
  declarations: [ReceivePaymentPage]
})
export class ReceivePaymentPageModule { }
