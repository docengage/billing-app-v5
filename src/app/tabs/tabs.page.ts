import { Component } from '@angular/core';
import { LocalStorageService } from '../service/local-storage.service';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';


export interface TabInterface {
  title: string;
  root: any;
  icon: string;
}

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  tabs: TabInterface[] = [];
  constructor(
    private localstorage: LocalStorageService,
    private authService: AuthService,
    private localStorge: LocalStorageService,
    private router: Router,
  ) {
    this.OnLoadCheckAuth();
  }
  OnLoadCheckAuth() {
    this.localstorage.getToken().then((tokenValue) => {
      if (tokenValue) {
        this.authService.getLoggedInUserInfo().then((data) => {
          this.authService.getProviderPermissions(data.uuid).then((accessData) => {
            this.localStorge.setAccessControl(accessData);
            this.setTabs(accessData);
          
        });
      });
      } else {
        this.router.navigate(['home']);
      }
    });

  }
  setTabs(accessControl) {
    let isAccess = false;
    if (accessControl) {
      accessControl = accessControl['permissions'];
      if (accessControl.Billing5674 && accessControl.Billingview) {
        this.router.navigate(['tabs/tabs/billing']);
        isAccess = true;
        this.tabs.push({ title: 'Payment', root: 'billing', icon: 'logo-usd' });
      } else {
        this.router.navigate(['tabs/tabs/about']);
      }
    }
    this.tabs.push({ title: 'My profile', root: 'about', icon: 'person' });
  }
}
