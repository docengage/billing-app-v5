import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, MenuController, NavController } from '@ionic/angular';
import { TabsPage } from '../tabs/tabs.page';
import { BillingPage } from '../billing/billing.page';
import { LocalStorageService } from '../service/local-storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  showSkip = true;

	@ViewChild('slides', { read: true, static: false }) ionSlides: IonSlides;
  constructor(
    private router: Router,
    public menu: MenuController,
    public storage: LocalStorageService,
    public navCtrl: NavController
    ) { }

  ngOnInit() {
  }
  startApp() {
    this.storage.setHasSeenTutorial('true');
    this.storage.getToken().then((tokenValue) =>{
      if(!tokenValue){
        this.router.navigate(['home']);
      }else{

        this.storage.getAccessControl().then((accessObj:any) =>{
          let accessControl = accessObj.permissions;
          if(accessControl){
            this.navCtrl.navigateForward(['tabs/tabs/billing'],accessControl);
          }else{
            this.router.navigate(['home']);
          }
        });
      }
    });
  }

  // onSlideChangeStart(slider: IonSlides) {
  //   this.showSkip = !slider.isEnd();
  // }

  // ionViewWillEnter(ionSlides: IonSlides) {
  //   this.ionSlides.update();
  // }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}
