import { Component, OnInit } from '@angular/core';
import { CallNumberService } from '../service/call-number.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage implements OnInit {

  constructor(private callNumberService: CallNumberService) { }
  callPatientMobile(mobileNo) {
    this.callNumberService.callToNumber(mobileNo);
  }

  ngOnInit() {
  }

}
