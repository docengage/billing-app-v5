import { Component } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';
import { Router } from '@angular/router';
import { Handlebar } from '../components/handlebar';
import { Todos } from '../service/todos';
import { MasterDataService } from '../service/master-data';
import { AppSettingsPage } from '../app-settings/app-settings.page';
import { ToastServiceData } from '../service/toast-service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loading: any;
  loginData = { loginId: '', password: '' };
  data: any;
  errorMessage: string;
  submitted: boolean = false;
  passwordType: string = 'password';
  showPass: boolean = false;
  isErrorExist: boolean = false;

  constructor(private authService: AuthService,
    private localstorage: LocalStorageService,
    private loadingCtrl: LoadingController,
    private router: Router,
    private modalCtrl: ModalController,
    private todos: Todos,
    private master: MasterDataService,
    private toastServ: ToastServiceData,
    private handlebar: Handlebar) {
    this.ionViewDidLoad();
  }

  ionViewDidLoad() {
    this.authService.isTokenValid().then((returnVal) => {
      this.localstorage.getToken().then((tokenValue) => {

        if (returnVal && tokenValue) {
          this.router.navigate(['tabs/tabs/billing']);
        } else {
          this.router.navigate(['home']);
        }
      });
    });
  }
  // login() {
  //   this.authService.login(this.loginData).then((result) => {
  //     this.router.navigate(['tabs']);
  //   }, (err) => {
  //     let errMsg = err.json().description;
  //     if (!errMsg) {
  //       errMsg = 'Not able to login! Please check your internet connection or connection URL.';
  //     }
  //   });
  // }

  async doLogin() {

    if (this.handlebar.isNotEmpty(this.loginData.loginId) && this.handlebar.isNotEmpty(this.loginData.password)) {
      this.showLoader();
      this.loading = await this.loadingCtrl.create({
        message: 'Authenticating...'
      });
      this.loading.present().then(() => {
        this.authService.login(this.loginData).then((result) => {
          this.isErrorExist = false;
          this.errorMessage = '';
          this.localstorage.setUserId(this.loginData.loginId);
          this.localstorage.setPassword(this.loginData.password);
          this.submitted = true;
          if (result && result[0] && result[0] === 'error') {
            this.loading.dismiss();
            alert('Invalid Credentials');
          } else {
            this.todos.recreateDB();
            this.data = result;
            this.localstorage.setToken(this.data.access_token);
            setTimeout(() => {
              this.loading.dismiss();
              this.authService.getLoggedInUserInfo().then((data) => {
                this.master.storeDataInLocalDB(data, 'loggedInProvider');
                this.localstorage.setLoggedInProvider(data.uuid);
                this.saveDeviceInformation(data.uuid);
                this.authService.getProviderPermissions(data.uuid).then((accessData) => {
                  this.localstorage.setAccessControl(accessData);
                  this.master.getAllMasterData();
                  if (data.uuid) {
                    this.router.navigate(['tabs']);
                  }
                });
              });
            }, 100);
          }
        }, (err) => {
          let errMsg = err.json().description;
          if (!errMsg) {
            errMsg = 'Not able to login! Please check your internet connection or connection URL.';
          }
          this.isErrorExist = true;
          this.errorMessage = errMsg;
          this.loading.dismiss();
        });
      });
    } else {
      this.submitted = true;
      this.isErrorExist = true;
      this.errorMessage = 'Please Enter User Credentials.';
    }
  }
  saveDeviceInformation(uuid) {
    this.localstorage.getDeviceToken().then((deviceToken) => {
      if (deviceToken && uuid) {
        this.authService.saveDeviceInfo(deviceToken, uuid);
      }
    });
  }

  forceLogout() {
    if (this.handlebar.isEmpty(this.loginData.loginId)) {
      alert('Please Provide login Id to do Forece Signout!');
    } else {
      this.authService.forceLogout(this.loginData.loginId).then(() => {
        this.isErrorExist = false;
        this.errorMessage = '';
      });
    }

  }

  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.passwordType = 'text';
    } else {
      this.passwordType = 'password';
    }
  }

  async showLoader() {
    this.loading = await this.loadingCtrl.create({
      message: 'Authenticating...'
    });
  }

  async openSettings() {
    this.localstorage.getEnvironment().then(async (env) => {
      const data = {
        "env": env
      };
      const modal = await this.modalCtrl.create({
        component: AppSettingsPage,
        componentProps: data
      });
      await modal.present();
    });

  }
}
