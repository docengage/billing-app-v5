import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, NavParams } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientService } from './service/http-client.service';
import { HttpClient } from './service/HttpClient';
import { Handlebar } from './components/handlebar';
import { Todos } from './service/todos';
import { ToastServiceData } from './service/toast-service';
import { MasterDataService } from './service/master-data';
import { ColorgeneratorComponent } from './components/colorgenerator/colorgenerator.component';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { ComponentsModule } from './components/components.module';
import { Network } from '@ionic-native/network/ngx';
import { Push } from '@ionic-native/push/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SelectTransactionPage } from './select-transaction/select-transaction.page';



@NgModule({
  declarations: [AppComponent, SelectTransactionPage],
  entryComponents: [SelectTransactionPage],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, IonicStorageModule.forRoot(), HttpModule, ComponentsModule],
  providers: [
    CallNumber,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    IonicStorageModule,
    HttpClientService,
    HttpClient,
    Handlebar,
    Todos,
    ToastServiceData,
    MasterDataService,
    ColorgeneratorComponent,
    Network,
    Push
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
