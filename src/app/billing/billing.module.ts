import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillingPageRoutingModule } from './billing-routing.module';

import { BillingPage } from './billing.page';
import { ReceivePaymentPage } from '../receive-payment/receive-payment.page';
import { PatientInvoiceListComponent } from '../components/patient-invoice-list/patient-invoice-list.component';
import { ReceiveWalletPaymentComponent } from '../components/receive-wallet-payment/receive-wallet-payment.component';
import { ComponentsModule } from '../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillingPageRoutingModule,
    ComponentsModule
  ],
  declarations: [BillingPage, ReceivePaymentPage, PatientInvoiceListComponent, ReceiveWalletPaymentComponent],
  entryComponents: [ReceivePaymentPage, ReceiveWalletPaymentComponent, PatientInvoiceListComponent]
})
export class BillingPageModule { }
