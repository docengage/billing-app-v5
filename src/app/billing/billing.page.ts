import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, Events } from '@ionic/angular';
import { ReceivePaymentPage } from '../receive-payment/receive-payment.page';
import { AuthService } from '../service/auth.service';
import { Handlebar } from '../components/handlebar';
import { ToastServiceData } from '../service/toast-service';
import { Todos } from '../service/todos';
import { LocalStorageService } from '../service/local-storage.service';
import { SelectTransactionPage } from '../select-transaction/select-transaction.page';
import * as moment from 'moment';
import { PatientInvoiceListComponent } from '../components/patient-invoice-list/patient-invoice-list.component';
import { ReceiveWalletPaymentComponent } from '../components/receive-wallet-payment/receive-wallet-payment.component';



@Component({
  selector: 'app-billing',
  templateUrl: './billing.page.html',
  styleUrls: ['./billing.page.scss'],
})
export class BillingPage implements OnInit {
  items: any;
  invoiceTog: any;
  searchTerm: any;
  totalPages: number;
  currentPage: number = 1;
  currentPatientPage: number = 1;
  totalPatientPages: number = 1;
  dateFilterType: String = "date_created";
  accessControl: any;
  noRecordFound: string;
  invoices: any;
  patients: any;
  searchPatient: any;
  invoiceShow: boolean = true;
  walletColor: boolean = false;
  invOldIndex: number = -1;
  loadProgress: number = 0;
  showSpinner: boolean = false;
  showInvSpinner: boolean = false;
  noPatientRecordFound: any;
  showPatInvoice: boolean = true;
  showContent: boolean = false;

  constructor(private router: Router,
    public modalCtrl: ModalController,
    private localstorage: LocalStorageService,
    private authService: AuthService,
    public handlebar: Handlebar,
    private toastServ: ToastServiceData,
    public todoService: Todos,
    public events: Events) { this.ionViewDidLoad(); }

  ngOnInit() {

  }

  ionViewDidLoad() {
    this.localstorage.getAccessControl().then((accessObj: any) => {
      this.accessControl = accessObj.permissions;
      if (this.accessControl) {
        if (this.accessControl.Billing5674 && this.accessControl.Billingview) {
          this.invoiceShow = true;
        } else {
          this.walletPay();
        }
      }

    });
    this.authService.isTokenValid().then((returnVal) => {
      this.items = [];
      this.totalPages = 0;
      this.currentPage = 1;
      this.invoices = [];
    });
  }
  invoiceToggel() {
    alert(this.invoiceTog);
  }
  invoicePay() {
    this.authService.getLoggedInUserInfo().then((data) => {
      this.authService.getProviderPermissions(data.uuid).then((accessData) => {
        this.localstorage.setAccessControl(accessData);
        this.accessControl = accessData['permissions'];
        if (this.accessControl) {
          if (this.accessControl.Billing5674 && this.accessControl.Billingview) {
            this.invoiceShow = true;
            this.walletColor = false;
            this.noPatientRecordFound = "";
          } else {
            this.toastServ.presentToast("You didn't have permission to view Invoices");
          }
        }
      });
    });
  }
  walletPay() {
    this.authService.getLoggedInUserInfo().then((data) => {
      this.authService.getProviderPermissions(data.uuid).then((accessData) => {
        this.localstorage.setAccessControl(accessData);
        this.accessControl = accessData['permissions'];
        if (this.accessControl) {
          if (this.accessControl.Patientview && this.accessControl.Patient5674) {
            this.showSpinner = true;
            this.noRecordFound = "";
            this.invoiceShow = false;
            this.walletColor = true;
            this.currentPatientPage = 1;
            this.patients = [];
            if (this.accessControl.Billing5674 && this.accessControl.Billingview) {
              this.showPatInvoice = true;
            } else {
              this.showPatInvoice = false;
            }
            this.getPatients();
          } else {
            this.toastServ.presentToast("You didn't have permission to view Patients List");
          }
        }
      });
    });
  }
  showInvContentData(index) {
    this.invoices[index].showContent = !this.invoices[index].showContent
    let invoiceId = this.invoices[index].invoice_id;
    if (this.invoices[index].showContent) {
      this.getInvoiceItems(invoiceId, index);
    }
    if (this.invOldIndex !== -1 && this.invOldIndex !== index) {
      this.invoices[this.invOldIndex].showContent = false;
    }
    this.invOldIndex = index;
  }
  showPatContentData(index) {
    this.patients[index].showContent = !this.patients[index].showContent
  }
  getInvoiceItems(invoiceId, index) {
    this.authService.getInvoiceItems(invoiceId).then((data) => {
      this.invoices[index].invoiceItems = data[0].length;
    });
  }
  async addWalletAmount(patient, walletAmount) {
    let data = {
      patient: patient,
      wallet: walletAmount
    };
    let modal = await this.modalCtrl.create({
      component: ReceiveWalletPaymentComponent,
      componentProps: data
    });
    await modal.present();
  }
  getWalletAmount(patient) {
    this.authService.getWalletAmount(patient.patientUhid).then((data) => {
      var walletAmount: any = 0;
      if (data) {
        walletAmount = data;
      } else {
        walletAmount = 0;
      }
      this.addWalletAmount(patient, walletAmount);
    });
  }
  async receivePayment(item) {
    let data1 = {
      item: item
    }
    let receivePayment = await this.modalCtrl.create({
      component: ReceivePaymentPage,
      componentProps: data1
    });

    receivePayment.onDidDismiss().then((data) => {
      if (data) {
        this.invoices = [];
        this.getInvoices();
      }
    });
    return await receivePayment.present();
  }
  async selectTransaction(item) {
    let data = {
      item: item
    }
    let selectTransaction = await this.modalCtrl.create({
      component: SelectTransactionPage,
      componentProps: data
    });

    return await selectTransaction.present();
  }
  onSearchInv() {
    this.showInvSpinner = true;
    if (this.handlebar.isEmpty(this.searchTerm)) {
      this.showInvSpinner = false;
      this.invoices = [];
      this.noRecordFound = "";
    } else {
      this.invoices = [];
      this.currentPage = 1;
      this.getInvoices();
    }
  }
  onSearchPatient() {
    this.showSpinner = true;
    this.patients = [];
    this.currentPatientPage = 1;
    this.getPatients();

  }
  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }

  getPatients() {
    let term = this.searchPatient;
    this.authService.searchPatientsList(term, this.currentPatientPage).then((data) => {
      this.patients = [];
      if (data.patients) {
        for (let i = 0; i < data.patients.length; i = i + 1) {
          data.patients[i].showContent = false;
          if (this.handlebar.isNotEmpty(data.patients[i].registeredOn)) {
            data.patients[i].registeredDate = moment(data.patients[i].registeredOn).format('DD MMM, HH:mm a');
          }
          this.patients.push(data.patients[i]);
        }
        this.totalPatientPages = data.totalPages;
        this.showSpinner = false;
        this.noPatientRecordFound = "";
      } else {
        this.noPatientRecordFound = "No Records Found";
      }
    });
  }
  getInvoices() {
    this.authService.getLoggedInUserInfo().then((data) => {
      this.authService.getProviderPermissions(data.uuid).then((accessData) => {
        this.localstorage.setAccessControl(accessData);
        this.accessControl = accessData['permissions'];
        if (this.accessControl) {
          if (this.accessControl.Billing5674 && this.accessControl.Billingview) {
            let term = this.searchTerm;
            if (this.handlebar.isEmpty(term)) {
              term = "0";
            }
            this.authService.findInvoice(term, this.currentPage, 0).then((data) => {
              this.invoices = [];
              if (data && data.invoice.length !== 0) {
                var invoices = data.invoice;
                for (let i = 0, I = invoices.length; i < I; i += 1) {
                  invoices[i].showContent = false;
                  invoices[i].invoiceItems = 0;
                  if (this.handlebar.isNotEmpty(invoices[i].due_date1)) {
                    try {
                      invoices[i].formatedDate = moment(invoices[i].due_date1).format("DD-MM-YYYY");
                    } catch (error) {
                      invoices[i].formatedDate = "";
                    }
                  }
                  this.invoices.push(invoices[i]);
                  this.totalPages = this.invoices[i].full_count / 15;
                  this.noRecordFound = "";
                  this.showInvSpinner = false;
                }
              } else {
                this.invoices = [];
                this.showInvSpinner = false;
                this.noRecordFound = "No Records Found";
              }

            });
          } else {
            this.toastServ.presentToast("You didn't have permission to view Invoices");
          }
        }
      });
    });
  }
  async showPatientInvoice(patient) {
    let data = {
      patient: patient,
    };
    let modal = await this.modalCtrl.create({
      component: PatientInvoiceListComponent,
      componentProps: data
    });
    await modal.present();
  }

  doRefresh(event) {
    if (this.handlebar.isNotEmpty(this.searchTerm) || this.handlebar.isNotEmpty(this.invoices)) {
      this.invoices = [];
      this.currentPage = 1;
      this.getInvoices();
      setTimeout(() => {
        event.target.complete();
        this.toastServ.presentToast("Invoice List Refreshed");
      }, 1000);
    } else {
      event.target.complete();
    }
  }
  doRefreshPatient(event) {
    this.patients = [];
    this.currentPatientPage = 1;
    this.getPatients();
    setTimeout(() => {
      event.target.complete();
      this.toastServ.presentToast("Patient List Refreshed");
    }, 1000);
  }

  doInfinite(event) {
    this.currentPage = Number(this.currentPage) + 1;
    setTimeout(() => {
      setTimeout(() => {
        if (this.currentPage <= this.totalPages) {
          this.getInvoices();
        }
        event.target.complete();
      }, 500);
    })
  }
  doInfinitePatient(event) {
    this.currentPatientPage = Number(this.currentPatientPage) + 1;
    setTimeout(() => {
      setTimeout(() => {
        if (this.currentPatientPage <= this.totalPatientPages) {
          this.getPatients();
        }
        event.target.complete();
      }, 500);
    })
  }

}
