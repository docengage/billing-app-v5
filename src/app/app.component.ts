import { Component } from '@angular/core';

import { Platform, Events, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './service/auth.service';
import { ToastServiceData } from './service/toast-service';
import { LocalStorageService } from './service/local-storage.service';
import { Subscription, from } from 'rxjs';
import { SupportPage } from './support/support.page';
import { AboutPage } from './about/about.page';
import { HomePage } from './home/home.page';
import { NavController } from '@ionic/angular';
import { BillingPage } from './billing/billing.page';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { Router, RouterEvent } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';


export interface PageInterface {
  title: string;
  url?: string;
  pageName: string;
  component: any;
  tabComponent?: any;
  index?: number;
  icon: string;
  logsOut?: boolean;
}

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  rootPage: any;
  accessControl: any;
  connected: Subscription;
  disconnected: Subscription;
  selectedUrl = '/tabs/tabs/billing';
  //Tab Nivagation page Interface added here...
  appPages: PageInterface[] = [];
  loggedInPages: PageInterface[] = [
    { title: 'My Profile', url: '/tabs/tabs/about', pageName: 'AboutPage', component: AboutPage, icon: 'person' },
    { title: 'Support', url: '/support', pageName: 'SupportPage', component: SupportPage, icon: 'chatboxes' },
  ];
  logout: PageInterface[] = [{ title: 'Logout', pageName: 'HomePage', component: HomePage, icon: 'log-out', logsOut: true }];

  constructor(
    public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    private localStorge: LocalStorageService, public events: Events,
    private router: Router,
    public alertCtrl: AlertController,
    private authService: AuthService,
    private network: Network,
    public toastService: ToastServiceData,
    public push: Push,
    public navCtrl: NavController
  ) {
    this.router.events.subscribe((event: RouterEvent) => {
      this.authService.checkIsLoggedIn().then((returnVal) => {
        if(returnVal){
        if (this.appPages.length === 0 && event.url &&  !this.accessControl) {
            this.authService.getLoggedInUserInfo().then((data) => {
              this.authService.getProviderPermissions(data.uuid).then((accessData) => {
                this.localStorge.setAccessControl(accessData);
                this.accessControl = accessData['permissions'];
                this.setAppPages();
              });
            });
        }
        if (event.url) {
          this.selectedUrl = event.url;
        }
      }
      });
    });
    this.localStorge.getHasSeenTutorial()
      .then((hasSeenTutorial) => {
        if (hasSeenTutorial) {
          this.rootPage = '/home';
        } else {
          this.rootPage = '/tutorial';
          this.navCtrl.navigateForward(['/tutorial']);
        }
        this.initializeApp();
      });
  }
  setAppPages() {
    this.appPages = [];
    if(this.accessControl){
      if (this.accessControl.Billing5674 && this.accessControl.Billingview) {
       this.appPages.push({ title: 'Payment', url: '/tabs/tabs/billing', pageName: 'BillingPage',
                           component: BillingPage, tabComponent: BillingPage, index: 0, icon: 'logo-usd' });
      }
    }
  }
  initializeApp() {
    this.authService.checkIsLoggedIn().then((returnVal) => {
      if (returnVal) {
        this.navCtrl.navigateForward(['/tabs/tabs/billing']);
      } else {
        this.navCtrl.navigateBack(this.rootPage);
      }
    });
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.networkCheck();
      this.initPushNotification();
    });
  }
  networkCheck() {
    this.network.onConnect().subscribe(() => {
      this.localStorge.getIsOnline().then((isOnline) => {
        if (!isOnline) {
          this.localStorge.setIsOnline(true);
          let networkType = this.network.type;
          if (networkType && networkType != null && networkType != 'null') {
            networkType = " via " + networkType;
          } else {
            networkType = "";
          }
          this.toastService.presentToast("All good! Your Internet connection is back " + networkType + "...", false, 10000);
        }
      });
    });
    this.network.onDisconnect().subscribe(() => {
      this.localStorge.getIsOnline().then((isOnline) => {
        if (isOnline) {
          this.localStorge.setIsOnline(false);
          this.toastService.presentToast("It looks like you've gone offline. Check your Internet connection.", false, 1000);
        }
      });
    });
  }
  initPushNotification() {
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: '303909800001'
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      this.localStorge.setDeviceToken(data.registrationId);
    });

    pushObject.on('notification').subscribe(async (data: any) => {
      console.log('message -> ' + data.message);
      //if user using app and push notification comes
      if (data.additionalData.foreground) {
        // if application open, show popup
        let confirmAlert = await this.alertCtrl.create({
          header: 'Notification',
          message: data.message,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }]
        });
        confirmAlert.present();
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        this.navCtrl.navigateForward('tabs', data.message);
        console.log('Push notification clicked');
      }
    });

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }

  // Accroding to index element page component and parameters willl be rendered from here.
  logoutFromApp(page: PageInterface) {
    let params = {};
    if (page.index) {
      params = { tabIndex: page.index };
    }
    if (page.logsOut === true) {
      this.authService.logout().then((data) => {
        if (data === 'Success') {
          // Give the menu time to close before changing to logged out
          this.localStorge.clearStorage().then(() => {
            this.router.navigate(['home']);
          });
        }
      });
    }
  }
}
