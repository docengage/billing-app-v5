import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LocalStorageService } from './local-storage.service';
import { HttpClientService } from './http-client.service';
import { Router } from '@angular/router';
import { ToastServiceData } from './toast-service';
import { Todos } from './todos';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Handlebar } from 'src/app/components/handlebar';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = false;

  constructor(private https: Http, private localstorage: LocalStorageService,
    public app: Router,
    private toastServ: ToastServiceData,
    private handlebar: Handlebar,
    private todos: Todos,
    private http: HttpClientService) { }

  login(credentials) {
    return new Promise((resolve, reject) => {
      if (this.handlebar.isNotEmpty(credentials.loginId) && this.handlebar.isNotEmpty(credentials.password)) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.localstorage.getURLByEnv().then((baseURL) => {
          this.https.post(baseURL + 'mobileLoginAction', JSON.stringify(credentials), { headers })
            .subscribe(res => {
              try {
                resolve(res.json());
              } catch (err) {
                const obj = { 0: 'error', 1: 'Please Check Your Credentials and Try Login Again.' };
                resolve(obj);
              }
            }, (err) => {
              reject(err);
            });
        });
      }else{
        const obj = { 0: 'error', 1: 'Please Check Your Credentials and Try Login Again.' };
        resolve(obj);
      }
    });
  }
  doReLogin(): Promise<any> {
    const loginData = { loginId: '', password: '' };
    return this.localstorage.getUserId().then((userId) => {
      loginData.loginId = userId;
      return this.localstorage.getPassword().then((password) => {
        loginData.password = password;
        return this.login(loginData).then((result) => {
          const data: any = result;
          this.localstorage.setToken(data.access_token);

        });
      });
    });
  }
  isTokenValid(): Promise<any> {
    return this.localstorage.getLoggedInProvider().then((uuid) => {
      return this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {

          return this.localstorage.getURLByEnv().then((baseURL) => {

            if (baseURL) {
              this.http.get(baseURL + 'api/v1/checkIsTokenValid/' + uuid).then((obj) => {
                obj.map(res => res.json())
                  .catch((error: any) => {
                    return this.doReLogin().then(() => {
                      return true;
                    });
                  })
                  .subscribe(data => {
                    return true;
                  }, (error) => {
                    return this.doReLogin().then((data) => {
                      return true;
                    }, (err) => {
                      alert('UnAuthorised Login');
                      this.app.navigate(['home']);
                    });
                  });
              });
            } else {
              return false;
            }
          });
        } else {
          return false;
        }
      });
    });
  }
  post(url, data) {
    return new Promise((resolve, reject) => {
      this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          resolve(this.postData(url, data));
        } else {
          this.toastServ.presentToast('You are saving in offline Mode. Check your Internet connection.', false, 10000);
          reject();
        }
      });
    });
  }
  postData(url, data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.checkIsLoggedIn().then((isLoggedIn) => {
        if (isLoggedIn) {

          return this.localstorage.getURLByEnv().then((baseURL) => {
            this.http.post(baseURL + url, data).then((obj) => {
              obj.map(res => res.json())
                .subscribe(data => {
                  resolve(data);
                }, (error) => {
                  resolve(error);
                });
            });
          });
        } else {
          reject();
        }
      });
    });
  }

  offlinePost(url, data, isPushFromDb, type): Promise<any> {
    return new Promise((resolve, reject) => {
      this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          resolve(this.postData(url, data));
        } else {
          if (!isPushFromDb) {
            this.toastServ.presentToast('It looks like you\'ve gone offline. Check your Internet connection.', false, 10000);
            resolve(this.todos.postToLocalDb(data, type));
          }
        }
      });
    });

  }
  get(url, returnObj): Promise<any> {
    return this.localstorage.getIsOnline().then((isOnline) => {
      if (isOnline) {
        return this.getData(url, 0).then((returnData) => {
          return returnData;
        });
      } else {
        alert('You are offline');
      }
    });
  }
  checkIsLoggedIn(): Promise<any> {
    return this.localstorage.getToken().then((tokenValue) => {
      if (tokenValue) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
      return this.isLoggedIn;
    });

  }
  getData(url, attempt) {
    return new Promise((resolve, reject) => {

      this.checkIsLoggedIn().then((isLoggedIn) => {
        if (isLoggedIn) {

          return this.localstorage.getURLByEnv().then((baseURL) => {
            this.http.get(baseURL + url).then((obj) => {
              obj.map(res => res.json())
                .catch(() => {
                  resolve('false');
                })
                .subscribe(data => {
                  resolve(data);
                }, (error) => {
                  resolve(error);
                });
            });
          });
        } else {
          resolve('false');
        }
      });
    });
  }
  logout(): Promise<any> {
    let data = {};
    return this.isTokenValid().then((returnVal) => {
      return this.localstorage.getLoggedInProvider().then((providerUUID) => {
        return this.localstorage.getIsOnline().then((isOnline) => {
          if (isOnline) {
            this.deactivateDeviceInfo(providerUUID);
            this.todos.destroyDB();
            return this.post('mobileLogout', data);
          } else {
            this.toastServ.presentToast("You Can't Logout in offline Mode. Check your Internet connection.", false, 10000);
            return false;
          }
        });
      });
    });

  }

  forceLogout(loginId): Promise<any> {
    const data = {
      loginId
    };
    return this.localstorage.getLoggedInProvider().then((providerUUID) => {
      return this.localstorage.getIsOnline().then((isOnline) => {
        if (isOnline) {
          this.deactivateDeviceInfo(providerUUID);
          return this.forceLogoutPost('mobileForceLogout', data);
        } else {
          this.toastServ.presentToast('You Can\'t Logout in offline Mode. Check your Internet connection.', false, 10000);
          return false;
        }
      });
    });

  }
  forceLogoutPost(url, data) {
    return new Promise((resolve, reject) => {

      return this.localstorage.getURLByEnv().then((baseURL) => {
        this.https.post(baseURL + url, data).subscribe(data => {
          resolve(data);
        }, (error) => {
          resolve(error);
        });
      });
    });

  }
  deactivateDeviceInfo(uuid): Promise<any> {
    return this.localstorage.getDeviceToken().then((deviceToken) => {
      const data = {
        deviceToken,
        providerUUID: uuid
      };
      return this.post('deactivateDevice', data);

    });
  }
  getLoggedInUserInfo(): Promise<any> {
    return this.get('api/v1/whoami', 'whoami');
  }
  getProviderInfo(): Promise<any> {
    return this.localstorage.getLoggedInProvider().then((uuid) => {
      return this.get('api/v1/providers/' + uuid, '');
    });
  }
  findInvoice(searchTerm: any, currentpage: any, patientId): Promise<any> {
    if (patientId == 0) {
      patientId = 'all';
    }
    return this.get('patientInvoice/list/Account/0/0/0/' + currentpage + '/' + encodeURIComponent(searchTerm) + '/' + patientId + '/all/all/json', 'invoices');
  }
  saveDeviceInfo(deviceToken, uuid): Promise<any> {
    const data = {
      deviceToken,
      providerUUID: uuid,
      deviceType: 'BillingApp'
    };
    return this.post('saveDevice', data);

  }
  getInvoiceItems(invoiceId) {
    return this.get('getInvoiceLineItems/' + invoiceId, 'invoicesItem');
  }
  getPaymentModes() {
    return this.get('getPaymentModes', 'paymentModes');
  }
  getWalletAmount(uhid) {
    return this.get('getPatientAdvanceAmount/' + uhid + '/againstInvoice', 'paymentModes');
  }
  addWalletAmount(walletPayment: any, isPushFromDb: boolean) {
    return this.offlinePost('addAdvancePayment/' + walletPayment.uhid, walletPayment, isPushFromDb, "invoiceData");
  }
  payInvoiceBill(recievePayment: any, isPushFromDb: boolean) {
    return this.offlinePost('addPatientPayment/' + recievePayment.uhid, recievePayment, isPushFromDb, "invoiceData");
  }
  getLeads(fromDate, toDate, statusValues, enqSourceFilVal, searchValue, pageno, dateFilterType): Promise<any> {
    if (this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    searchValue = "&leadSearch=" + searchValue;
    let offset = (Number(pageno) - 1) * 15;
    return this.get('api/v1/leads?&from=' + fromDate + '&to=' + toDate + searchValue + "&offset=" + offset + "&dateFilterType=" + dateFilterType +
      "&statusValues=" + statusValues + "&enqSourceFilVal=" + enqSourceFilVal, "leads");
  }
  getPrintSettingsInfo(): Promise<any> {
    return this.get('api/v1/print/settings', "");
  }
  getMyPatientsList(): Promise<any> {
    return this.get('api/v1/patients', "");
  }

  getPatientTimeLineData(uhid, pageno, includeFilters): Promise<any> {
    let offset = (Number(pageno) - 1) * 15;
    return this.get('api/v1/timeline/overview/items/' + uhid + "?type=" + includeFilters + "&offset=" + offset, "");
  }


  getPatientTimeLineItemDetails(referenceId, referenceType): Promise<any> {
    return this.get('api/v1/timeline/items/details/' + referenceId + '/' + referenceType, "");
  }
  getPatientInfo(uuid): Promise<any> {
    return this.get('api/v1/patients?uuid=' + uuid + '&limit=1&offset=0', "");
  }
  getPatientInfoLight(uuid): Promise<any> {
    return this.get('api/v1/patients/light/' + uuid, "");
  }

  getLeadTimeLineData(leadId, pageno, includeFilters): Promise<any> {
    return this.get('api/v1/leads/' + leadId + '/timeline/items', "");
  }

  getLeadsInfo(fromDate, toDate, searchValue, pageno): Promise<any> {
    return this.getLeads(fromDate, toDate, "", "", "", 1, "date_created");
  }
  getLeadConfig(organizationId): Promise<any> {
    return this.get('api/v1/organizations/' + organizationId + '/leads/preferences', "leadConfig");
  }
  getInsuCompanies(): Promise<any> {
    return this.get('getAllInsuranceCompanies', "insuCompanies");
  }
  getRepeatFollowupReasons(): Promise<any> {
    return this.get('get/allRepeatFollowups/Active', "repeatFollowupData");
  }
  getScheduleProvidersBySpeciality(ccid): Promise<any> {
    return this.get('getScheduleProvidersBySpeciality/' + ccid + '/false/all', "providers");
  }
  getAllStates(): Promise<any> {
    return this.get('getAllStates', "");
  }
  getAllCitiesByStates(state): Promise<any> {
    return this.get('getAllCitiesByStates/' + state, "");
  }
  searchPatientsList(searchValue, pageno): Promise<any> {
    if (this.handlebar.isEmpty(searchValue)) {
      searchValue = "$";
    }
    let offset = (Number(pageno) - 1) * 15;
    return this.get('api/v1/patients?patientSearch=' + searchValue + "&offset=" + offset, "patients");

  }

  getCareCentersMasterData(city): Promise<any> {
    return this.localstorage.getLoggedInProvider().then((uuid) => {
      return this.get('api/v1/master/care_center/' + uuid + '?city=' + city, "careCenters");
    });
  }
  getCareCentersMasterDataById(careCenterId): Promise<any> {
    return this.get('api/v1/master/care_center/byid/' + careCenterId, "");
  }

  getCitiesMasterData(): Promise<any> {
    return this.get('allCareCentersCities', "cities");
  }

  getProviderPermissions(providerUUId): Promise<any> {
    return this.get('api/v1/mypermission/' + providerUUId, "");
  }
  getHCRMCustomFieldNames(): Promise<any> {
    return this.get('customFieldConfig/hcrm', "");
  }
  getEnquiryTypes(): Promise<any> {
    return this.get("api/v1/master/enquiryType", "enquiryTypes");
  }
  getEnquirySubTypes(type): Promise<any> {
    return this.get("getAllEnquirySubTypeByType/" + type, type + "SubType");
  }


  getRelationships(): Promise<any> {
    return this.get("api/v1/master/relationships", "relationships");
  }
  getPrimaryPhysicians(): Promise<any> {
    return this.get("api/v1/master/primaryPhysicians", "primaryPhysicians");
  }
  getActivities(): Promise<any> {
    return this.get("api/v1/master/activities", "activities");
  }
  getSpecialities(): Promise<any> {
    return this.get("getAllSpecialities/Active", "specialities");
  }
  getAllStatus(): Promise<any> {
    return this.get("settings/enquiryStatus/Active", "status");
  }
  getSubStatusByStatus(status): Promise<any> {
    return this.get("api/v1/master/subStatus/" + status, "subStatus");
  }
  getPriorities(): Promise<any> {
    return this.get("api/v1/master/priorities", "priorities");
  }
  getExHospitals(): Promise<any> {
    return this.get("allexteranhospitalnames", "exHospital");
  }
  getExProviders(): Promise<any> {
    return this.get("allexternalprovidernames", "exProviders");
  }
  getAllConsultants(): Promise<any> {
    return this.get("getallorgconsultants", "consultants");
  }

  getCaseCategories(): Promise<any> {
    return this.get("get/allActiveVisitTypeTemplates", "caseCategories");
  }


  getServiceCategories(): Promise<any> {
    return this.get("getservicesType", "serviceCategories");
  }
  getSources(): Promise<any> {
    return this.get("getPtReferneces", "enquirySources");
  }

  getSourcesBySubType(enquiryType, enquirySubType): Promise<any> {
    return this.get('getAllEnquirySourceBySubType/' + enquiryType + '/' + enquirySubType, "enquirySources");
  }

  getDiscounts(): Promise<any> {
    return this.get("getDiscounts/all", "discounts");
  }
  getLocalityByCity(city): Promise<any> {
    return this.get("getByLocalityName/" + city, "discounts");
  }
  getServiceListByCareCenterId(careCenterId): Promise<any> {
    var type = "mobile"
    return this.get("serviceListByCriteria/all/" + careCenterId + "?type=" + type, "serviceList");
  }

  getServiceListByServiceType(careCenterId, serviceType): Promise<any> {
    var type = "mobile"
    return this.get("serviceListByCriteria/" + serviceType + "/" + careCenterId + "?type=" + type, "serviceTypeList");
  }
  getSearchListByServiceType(careCenterId, searchTerm, pgno, limit): Promise<any> {
    var type = "mobile"
    return this.get("serviceListByCriteria/all/" + careCenterId + "?searchTerm=" + searchTerm + "&pgno=" + pgno + "&limit=" + limit + "&type=" + type, "serviceSearchList");
  }

  getOrgPreferences(organizationId): Promise<any> {
    return this.localstorage.getOrgPreferences().then((pref) => {
      if (pref) {
        return pref;
      } else {
        return this.get('api/v1/org/' + organizationId + '/preferences', "").then((prefData) => {
          this.localstorage.setOrgPreferences(prefData["preferences"]);
          return prefData["preferences"];
        });
      }
    });
  }
  saveLeadInfo(data, isPushFromDb): Promise<any> {

    return this.offlinePost('api/v2/push_lead', data, isPushFromDb, "leads");

  }

  saveActivityData(data, leadId, isPushFromDb): Promise<any> {
    return this.offlinePost('api/v1/patch/leads/' + leadId, data, isPushFromDb, "LeadActivity");
  }
  saveLeadNote(data, leadId, isPushFromDb): Promise<any> {
    return this.offlinePost('api/v1/leads/' + leadId + '/notes', data, isPushFromDb, "LeadNotes")
  }
  statusUpdate(data, isPushFromDb): Promise<any> {
    return this.offlinePost('api/v1/patch/leads/' + data.leadId, data, isPushFromDb, "LeadStatus");
  }
  statusUpdtForRepeatFollowup(data, leadId, isPushFromDb): Promise<any> {
    return this.offlinePost('update/enquiry/preffereddate/' + leadId, data, isPushFromDb, "LeadStatus")
  }
  statusUpdateWithReason(data, leadId, isPushFromDb): Promise<any> {
    return this.offlinePost('api/v1/patch/leads/' + leadId, data, isPushFromDb, "LeadStatusWithReason")
  }
  disqualifyReasons(): Promise<any> {
    return this.get("get/allDisqualifyReasons/Active", "disqualifyReasons");
  }

  getLeadActivities(fromDate, toDate, searchValue, pageno): Promise<any> {
    return this.localstorage.getLoggedInProvider().then((providerUUID) => {
      if (this.handlebar.isEmpty(searchValue)) {
        searchValue = "$";
      }
      searchValue = "&leadName=" + searchValue;
      let offset = (Number(pageno) - 1) * 15;
      return this.get('api/v1/providers/' + providerUUID + '/leads/activities?from=' + fromDate + '&to=' + toDate + searchValue + "&offset=" + offset, "LeadActivity");
    });
  }


  updateActivityStatus(data, eventid, event): Promise<any> {
    let data0 = {
      status: data.status,
      generalNotes: data.generalNotes,
      type: event,
      _id: data._id,
      leadId: data.leadId
    };
    return this.post('api/v1/change/task-status/' + eventid, data0);

  }
  getNotifyTeams(): Promise<any> {
    return this.get('notifyProviders/all', "teams");
  }
  leadDocumentSave(leadid, byteArray, startDate, itemName, urifilename, mimetype) {
    let data = {
      "byteArray": byteArray,
      "startDate": startDate,
      "itemName": itemName,
      "leadid": leadid,
      "urifilename": urifilename,
      "mimetype": mimetype
    }
    return this.post('document/byteArray/Lead', data);
  }
}
