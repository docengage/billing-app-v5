import { Injectable } from '@angular/core';
import { Handlebar } from '../components/handlebar';
import { ToastServiceData } from './toast-service';
import { CallNumber } from '@ionic-native/call-number/ngx'; 

@Injectable({
  providedIn: 'root'
})
export class CallNumberService {

  constructor(public callNumber:CallNumber,private handlebar:Handlebar,
    private toastServ: ToastServiceData) { }
    callToNumber(mobileNo){
      if(this.handlebar.isEmpty(mobileNo)){
          this.toastServ.presentToast("Mobile Number is Empty!");
      }else{
          this.callNumber.callNumber(mobileNo, true)
          .then(() => console.log('Launched dialer!'))
          .catch(() => console.log('Error launching dialer'));
      }
  }
}
