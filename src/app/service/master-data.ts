import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';
import { AuthService } from './auth.service';
import { Handlebar } from 'src/app/components/handlebar';
import { Todos } from './todos';



@Injectable()
export class MasterDataService {
  data: any;
  organizationId: any;
  accessControl: any;
  constructor(private localstorage: LocalStorageService,
              private handlebar: Handlebar, private authService: AuthService,
              public todoService: Todos) { }

  getTimeLineFilters(): Promise<any> {
    return this.localstorage.getAccessControl().then((accessObj) => {
      return this.localstorage.getTracks().then((tracksCache) => {
        this.accessControl = accessObj;

        if (this.handlebar.isEmpty(tracksCache)) {

            const tracks = [];
            if (this.accessControl.permissions.Calendarview) {
              tracks.push({value: 'planneditem', label: 'Appointmet', isChecked: true});
            }
            if (this.accessControl.permissions.Activityview) {
              tracks.push({value: 'activity', label: 'Activity', isChecked: true});
            }
            if (this.accessControl.permissions.Vitalview) {
              tracks.push({value: 'vitals', label: 'Vitals', isChecked: true});
            }
            if (this.accessControl.permissions.Careplanview) {
              tracks.push({value: 'careplan', label: 'Careplan', isChecked: true});
            }
            if (this.accessControl.permissions.GeneralNotesview) {
              tracks.push({value: 'progressnote,instruction,clinicalnote,history', label: 'Notes', isChecked: true});
            }
            if (this.accessControl.permissions.Prescriptionview) {
              tracks.push({value: 'medication', label: 'Medication', isChecked: true});
            }
            if (this.accessControl.permissions.Radiologyview ||
              this.accessControl.permissions.Pathologyview) {
              tracks.push({value: 'laborder', label: 'Laborder', isChecked: true});
            }
            if (this.accessControl.permissions.Billingview) {
              tracks.push({value: 'estimated', label: 'Estimation', isChecked: true});
              tracks.push({value: 'invoiced', label: 'Invoice', isChecked: true});
              tracks.push({value: 'payments', label: 'Payments', isChecked: true});
            }
            tracks.push({value: 'files', label: 'Documents', isChecked: true});

            if (this.accessControl.permissions.DynamicFormview) {

              tracks.push({value: 'dynamicform-', label: 'Assessment', isChecked: true});
            }

            this.localstorage.setTracks(tracks);
            return tracks;
          } else {
            return tracksCache;
          }
        });

    });

  }
  getLeadTimeLineFilters(): Promise<any> {
    return this.localstorage.getAccessControl().then((accessObj) => {
      return this.localstorage.getLeadTracks().then((tracksCache) => {
        this.accessControl = accessObj;

        if (this.handlebar.isEmpty(tracksCache)) {

            const tracks = [];
            if (this.accessControl.permissions.Activityview) {
              tracks.push({value: 'activity', label: 'Activity', isChecked: true});
            }
            if (this.accessControl.permissions.GeneralNotesview) {
              tracks.push({value: 'progressnote,instruction,clinicalnote,history', label: 'Notes', isChecked: true});
            }
            tracks.push({value: 'files', label: 'Documents', isChecked: true});
            tracks.push({value: 'email', label: 'Emails', isChecked: true});
            tracks.push({value: 'sms', label: 'SMS', isChecked: true});

            this.localstorage.setLeadTracks(tracks);
            return tracks;
          } else {
            return tracksCache;
          }
        });

    });

  }
  pushObjectToElement(elements, data) {
    if (data) {
      for (let i = 0; i < Object.keys(data).length; i++) {
        const key: string = Object.keys(data)[i];
        if (data[key]) {
          for (let j = 0; j < data[key].length; j++) {
            elements.push(data[key][j]);
          }
        }
      }
    }
    return elements;
  }

    checkPermission(type, actionName) {
      return new Promise((resolve) => {
        this.localstorage.getAccessControl().then((accessObj: any) => {
          this.accessControl = accessObj.permissions;
          if (this.accessControl[type + '5674']) {
            resolve(this.accessControl[type + actionName]);
          } else {
            resolve(false);
          }
        });
      });

    }
    deDoubleValidation(numberValue, fieldName, checkNotEmpty) {
      let errorMessage = '';
      const regEx = /^\-?([0-9]+(\.[0-9]+)?|Infinity)$/;
      if (checkNotEmpty && numberValue === '') {
            errorMessage = errorMessage + fieldName + ' can\'t be empty.<br>';
        } else if (numberValue !== '') {
        if (regEx.test(numberValue) === false) {
            errorMessage = errorMessage + fieldName + ' should be a number.<br>';
        } else {
          if (numberValue !== '' && Math.ceil(numberValue) < 1) {
                errorMessage = errorMessage + fieldName + ' should be positive a value.<br>';
            }
        }
        }

      return errorMessage;
    }
    getLoggedInProviderInfo() {

      this.todoService.getToDosData('loggedInProvider').then((data0) => {
          if (data0) {
              this.organizationId = data0.organizationId;
          }
      });
  }

    getAllMasterData() {
      this.getLoggedInProviderInfo();
    }


    storeDataInLocalDB(data, type) {

      const newDoc = {
        _id: type,
        message: data,
        type,
        isSync: true
      };
      this.todoService.deleteAndAddTodoById(type, newDoc);
    }
    deIntegerValidation(numberValue, fieldName, checkNotEmpty) {
      let errorMessage = '';
      if (numberValue != '') {
        let number = /^-?[0-9]+$/;
        const regex = RegExp(number);
        if (regex.test(numberValue) === false) {
            const error = fieldName + ' should be number.<br>';
            errorMessage = errorMessage + error;
        } else if (checkNotEmpty) {
         if (numberValue.trim() === '') {
            const error = fieldName + ' is Required.<br>';
            errorMessage = errorMessage + error;
         }
        } else if (numberValue !== '' && Math.ceil(numberValue) < 1) {
            const error = fieldName + ' should be positive a value.<br>';
            errorMessage = errorMessage + error;
        }
      }
      return errorMessage;
    }
    hours_am_pm(time) {
      let hours: any = Number(time.match(/^(\d+)/)[1]);
      let min: any =  Number(time.match(/:(\d+)/)[1]);
      if (min < 10) { min = '0' + min; }
      if (hours < 12) {
          return hours + ':' + min + ' AM';
      } else  if (hours == 12) {
        return hours + ':' + min + ' PM';
      } else  if (hours == 24) {
        return  '00:' + min + ' AM';
      } else {
          hours = hours - 12;
          hours = (hours < 10) ? '0' + hours : hours;
          return hours + ':' + min + ' PM';
      }
  }
}
