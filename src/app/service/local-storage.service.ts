import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { stageEnv } from '../../environments/environment.stage';


@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    constructor(private storage: Storage) { }

    // store the email address
    setToken(token) {
        this.storage.set('token', token);
    }
    getToken(): Promise<string> {
        return this.storage.get('token').then((value) => {
            return value;
        });
    }
    setUserId(userId) {
        this.storage.set('userId', userId);
    }
    getUserId(): Promise<string> {
        return this.storage.get('userId').then((value) => {
            return value;
        });
    }
    setPassword(password) {
        this.storage.set('password', password);
    }
    getPassword(): Promise<string> {
        return this.storage.get('password').then((value) => {
            return value;
        });
    }
    setTracks(tracks) {
        this.storage.set('tracks', tracks);
    }
    getTracks(): Promise<string> {
        return this.storage.get('tracks').then((value) => {
            return value;
        });
    }
    setLeadTracks(tracks) {
        this.storage.set('leadtracks', tracks);
    }
    getLeadTracks(): Promise<string> {
        return this.storage.get('leadtracks').then((value) => {
            return value;
        });
    }
    setIsOnline(value) {
        this.storage.set('isOnline', value);
    }
    setCustomURL(customURL) {
        this.storage.set('customURL', customURL);
    }
    getCustomURL(): Promise<string> {
        return this.storage.get('customURL').then((value) => {
            return value;
        });
    }
    getIsOnline(): Promise<boolean> {
        return this.storage.get('isOnline').then((value) => {
            if (value == null) {
                return true;
            } else {
                return value;
            }
        });
    }
    setLoggedInProvider(uuid) {
        this.storage.set('loggedInProviderUUID', uuid);
    }
    getLoggedInProvider(): Promise<string> {
        return this.storage.get('loggedInProviderUUID').then((value) => {
            return value;
        });
    }
    setAccessControl(accessObj) {
        this.storage.set('accessControl', accessObj);
    }
    getAccessControl(): Promise<string> {
        return this.storage.get('accessControl').then((accessObj) => {
            return accessObj;
        });
    }
    setCustomFields(customFields) {
        this.storage.set('customFields', customFields);
    }
    getCustomFields(): Promise<string> {
        return this.storage.get('customFields').then((customFields) => {
            return customFields;
        });
    }
    setHasSeenTutorial(hasSeenTutorial) {
        this.storage.set('hasSeenTutorial', hasSeenTutorial);
    }
    getHasSeenTutorial(): Promise<string> {
        return this.storage.get('hasSeenTutorial').then((hasSeenTutorial) => {
            return hasSeenTutorial;
        });
    }
    setOrgPreferences(orgPreferences) {
        this.storage.set('orgPreferences', orgPreferences);
    }
    getOrgPreferences(): Promise<string> {
        return this.storage.get('orgPreferences').then((orgPreferences) => {
            return orgPreferences;
        });
    }
    setEnvironment(env) {
        this.storage.set('environment', env);
    }
    getEnvironment(): Promise<string> {
        return this.storage.get('environment').then((environment) => {
            return environment;
        });
    }
    setDeviceToken(deviceToken) {
        this.storage.set('deviceToken', deviceToken);
    }
    getDeviceToken(): Promise<string> {
        return this.storage.get('deviceToken').then((deviceToken) => {
            return deviceToken;
        });
    }
    setNoteSeq(noteCounter) {
        this.storage.set('noteCounter', noteCounter);
    }
    getNoteSeq(): Promise<string> {
        return this.storage.get('noteCounter').then((noteCounter) => {
            return noteCounter;
        });
    }
    setActivitySeq(counter) {
        this.storage.set('activityCounter', counter);
    }
    getActivitySeq(): Promise<string> {
        return this.storage.get('activityCounter').then((counter) => {
            return counter;
        });
    }
    getURLByEnv():Promise<string>{
        return this.storage.get('environment').then((env) => {
            if(env==="stageEnv"){
                return stageEnv.BASE_URL;
            }else if(env==="customEnv"){
                return this.getCustomURL().then((customURL) => {
                    return customURL;
                });
            }else{
                return environment.BASE_URL;
            }
        });
    }
    // clear the whole local storage
    clearStorage(): Promise<any> {
         return this.getEnvironment().then((environment) => {
            return this.getHasSeenTutorial().then((hasSeenTutorial) => {
                return this.getDeviceToken().then((deviceToken) => {
                    return this.getCustomURL().then((customURL) => {
                        return this.storage.clear().then(() => {
                            this.setHasSeenTutorial(hasSeenTutorial);
                            this.setEnvironment(environment);
                            this.setDeviceToken(deviceToken);
                            this.setCustomURL(customURL);
                        });
                    });
                });
            });
        });
    }
}
