import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { LocalStorageService } from './local-storage.service';



@Injectable()
export class HttpClient {

  constructor(private http: Http,
    private localstorage: LocalStorageService) { }

  createAuthorizationHeader(headers: Headers): Promise<any> {

    return this.localstorage.getToken().then((tokenValue) => {
      if (tokenValue) {
        headers.append('X-Auth-Token', tokenValue);
      }
    });
  }

  get(url): Promise<any> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() => {
      return this.http.get(url, {
        headers: headers
      });
    });
  }

  post(url, data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() => {
      return this.http.post(url, data, {
        headers: headers
      });
    });
  }

  put(url, data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() => {
      return this.http.put(url, data, {
        headers: headers
      });
    });
  }
  patch(url, data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.createAuthorizationHeader(headers).then(() => {
      return this.http.patch(url, data, {
        headers: headers
      });
    });
  }
}