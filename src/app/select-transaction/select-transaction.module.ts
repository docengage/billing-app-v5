import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectTransactionPageRoutingModule } from './select-transaction-routing.module';

import { SelectTransactionPage } from './select-transaction.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectTransactionPageRoutingModule
    ],
  declarations: [SelectTransactionPage]
})
export class SelectTransactionPageModule {}
