import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import { AuthService } from '../service/auth.service';
import { LocalStorageService } from '../service/local-storage.service';
import { PatientInvoiceListComponent } from '../components/patient-invoice-list/patient-invoice-list.component';
import { ReceiveWalletPaymentComponent } from '../components/receive-wallet-payment/receive-wallet-payment.component';

@Component({
  selector: 'app-select-transaction',
  templateUrl: './select-transaction.page.html',
  styleUrls: ['./select-transaction.page.scss'],
})
export class SelectTransactionPage implements OnInit {
  walletAmount: any;
  patient: any;
  accessControl: any;
  invoiceShow: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private localstorage: LocalStorageService,
    private authService: AuthService) {
    this.patient = this.navParams.data.item;
    this.getWalletAmount();
    this.ionViewDidLoad();
  }

  ionViewDidLoad() {
    this.localstorage.getAccessControl().then((accessObj: any) => {
      this.accessControl = accessObj.permissions;
      if (this.accessControl.Billing5674 && this.accessControl.Billingview) {
        this.invoiceShow = true;
      } else {
        this.invoiceShow = false;
      }
    });
  }

  getWalletAmount() {
    this.authService.getWalletAmount(this.patient.patientUhid).then((data) => {
      if (data) {
        this.walletAmount = data;
      } else {
        this.walletAmount = 0;
      }
    });
  }

  async addWalletAmount() {
    let data = {
      patient: this.patient,
      wallet: this.walletAmount
    };
    let modal = await this.modalCtrl.create({
      component: ReceiveWalletPaymentComponent,
      componentProps: data
    });
    await modal.present();
    modal.onDidDismiss().then((isUpdate) => {
      if (isUpdate) {
        this.getWalletAmount();
      }
    });
  }

  async showPatientInvoice() {
    let data = {
      patient: this.patient,
    };
    let modal = await this.modalCtrl.create({
      component: PatientInvoiceListComponent,
      componentProps: data
    });
    await modal.present();
  }
  dismiss(isUpdated, data?: any) {
    this.modalCtrl.dismiss(data, isUpdated);
  }
  ngOnInit() {
  }

}
