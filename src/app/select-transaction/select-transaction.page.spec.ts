import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectTransactionPage } from './select-transaction.page';

describe('SelectTransactionPage', () => {
  let component: SelectTransactionPage;
  let fixture: ComponentFixture<SelectTransactionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTransactionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectTransactionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
