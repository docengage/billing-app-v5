import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Handlebar } from '../components/handlebar';
import { AuthService } from '../service/auth.service';
import * as moment from 'moment';
import { LocalStorageService } from '../service/local-storage.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  providerInfo: any;
  constructor(public navCtrl: NavController,
    public localstorage: LocalStorageService,
    public handlebar: Handlebar,
    public authService: AuthService) { this.ionViewDidLoad(); }

  ionViewDidLoad() {
    this.authService.getProviderInfo().then((data) => {

      this.providerInfo = data;
      this.providerInfo.greetings = "Good " + this.getGreetingTime(moment());
    });


  }
  getGreetingTime(m) {
    let g = null; // return g
    if (!m || !m.isValid()) { return; }

    let split_afternoon = 12 // 24hr time to split the afternoon
    let split_evening = 17 // 24hr time to split the evening
    let currentHour = parseFloat(m.format("HH"));

    if (currentHour >= split_afternoon && currentHour <= split_evening) {
      g = "Afternoon";
    } else if (currentHour >= split_evening) {
      g = "Evening";
    } else {
      g = "Morning";
    }
    return g;
  }
  checkPatientHasImage(imagePath) {
    return this.handlebar.checkImagePath(imagePath);
  }

  ngOnInit() {
  }

}
