import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ToastServiceData } from '../service/toast-service';
import { LocalStorageService } from '../service/local-storage.service';

@Component({
  selector: 'app-app-settings',
  templateUrl: './app-settings.page.html',
  styleUrls: ['./app-settings.page.scss'],
})
export class AppSettingsPage implements OnInit {
  appBaseUrl: string = "environment";
  isCustomUrl: boolean = false;
  customURL: string = "";
  constructor(public navParams: NavParams,
    public modalCtrl: ModalController,
    private localstorage : LocalStorageService,public toastService: ToastServiceData) {
      const env = this.navParams.get('env');
      if(env && env!=null){
      this.appBaseUrl = env;
      this.getCustomURLValue();
      this.onChangeENV();
    } 
  }

  ngOnInit() {
  }
  getCustomURLValue(){
    this.localstorage.getCustomURL().then((customURL) =>{
      this.customURL = customURL;
    });
  }
  onChangeENV(){
    if(this.appBaseUrl==='customEnv'){
        this.isCustomUrl = true;
    }else{
        this.isCustomUrl = false;
    }
  }
  isValidURL(customURL){
    let pattern = new RegExp('^((http?:)?\\/\\/)?'+ // protocol
        '([a-zA-Z+]+[0-9+:?]+)|([0-9+]+[a-zA-Z+]+)$');
        
    if (!pattern.test(customURL)) {
        return false;
    } else {
        return true;
     
    }
  }
  saveEnvChanges(){
    if(this.isCustomUrl){
      let returnVal = this.isValidURL(this.customURL);
      if(returnVal){
        this.localstorage.setCustomURL(this.customURL);
        this.dismiss(true);
      }else{
        this.toastService.presentToast("Invalid Custom URL");
      }

    }else{
      this.localstorage.setCustomURL("");
      this.dismiss(true);
    }
  }
 

  dismiss(isupdated) {
    if(isupdated){
      this.localstorage.setEnvironment(this.appBaseUrl);
    }
    // using the injected ViewController this page
    // can "dismiss" itself and pass back data
    this.modalCtrl.dismiss(isupdated);
  }
}
