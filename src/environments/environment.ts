export const environment = {
    production: true,  // this value can be anything - will be replaced during build
    BASE_URL: 'https://apps.docengage.in/', // this value can be anything - will be replaced during build
};